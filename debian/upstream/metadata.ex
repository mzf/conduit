# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/conduit/issues
# Bug-Submit: https://github.com/<user>/conduit/issues/new
# Changelog: https://github.com/<user>/conduit/blob/master/CHANGES
# Documentation: https://github.com/<user>/conduit/wiki
# Repository-Browse: https://github.com/<user>/conduit
# Repository: https://github.com/<user>/conduit.git
